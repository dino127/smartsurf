$('.section_binhluan .btn-loadmore').click(function(){
  $('.section_binhluan .display').toggleClass('active');
})

$('.slide_hethong').slick({
  infinite: true,
  slidesToShow : 2,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  vertical: true,
  verticalSwiping: true,
  responsive: [
    {
      breakpoint: 1366,
      settings: {
        slidesToShow: 1.5,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});

window.onload = function() {
  var element = document.getElementById("loading");
  if (typeof(element) != 'undefined' && element != null) {
    setTimeout(function() {
      document.getElementById("loading").classList.add("finished");
    }, 200);
  }
}

//Thêm hieu ung cho chu slide home
var myclass = 'vis animated fadeInUp';
var slidehome = $('.gallery-home');
//$(".gallery-home .item:first-child").find('.item__txt_1, .item__txt_2, .btn_gr').addClass(myclass);
if (slidehome.length)
  slidehome.owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    items: 1,
    autoplay: true,
    autoplayTimeout: 6000,
    animateIn: 'zoomIn',
    animateOut: 'zoomOut',
    dots: true,
    mouseDrag: false
  });

  var tickyclass = 'vis animated fadeInUp';
  var gallerysticky = $('.gallery-sticky');
  //$(".gallery-home .item:first-child").find('.item__txt_1, .item__txt_2, .btn_gr').addClass(myclass);
  if (gallerysticky.length)
    gallerysticky.owlCarousel({
      loop: true,
      autoplay: true,
      items: 2,
      nav: true,
      autoplayHoverPause: true,
      animateOut: 'slideOutUp',
      animateIn: 'slideInUp'
    });

  var slidegioithieu = $('.gallery-gioithieu');
  //$(".gallery-home .item:first-child").find('.item__txt_1, .item__txt_2, .btn_gr').addClass(myclass);
  if (slidegioithieu.length)
    slidegioithieu.owlCarousel({
      loop: true,
      margin: 0,
      nav: false,
      items: 1,
      autoplay: true,
      autoplayTimeout: 6000,
      animateIn: 'flipInX',
      animateOut: 'flipOutX',
      dots: true,
      mouseDrag: false
    });
// slidehome.on('translated.owl.carousel', function(event) {
//   $(".gallery-home .item").find('.item__txt_1, .item__txt_2, .btn_gr').removeClass(myclass);
//   $(".gallery-home .active").find('.item__txt_1, .item__txt_2, .btn_gr').addClass(myclass);
// })
//Dừng lại khi hover chuột, play khi mouse out
// slidehome.mouseover(function() {
//   slidehome.trigger('stop.owl.autoplay');
// });
//
// slidehome.mouseleave(function() {
//   slidehome.trigger('play.owl.autoplay', [500]);
// });
//

var gallery_hethong = $('.gallery_hethong');
if (gallery_hethong.length)
  gallery_hethong.owlCarousel({
    loop: true,
    margin: 16,
    nav: true,
    autoplay: true,
    autoplayTimeout: 6000,
    items: 3,
    dots: false,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1
      },
      // breakpoint from 768 up
      768: {
        items: 2
      },
      // breakpoint from 1000 up
      1000: {
        items: 3
      }
    }
  })

var gallery_news_event = $('.gallery_news_event');
if (gallery_news_event.length)
  gallery_news_event.owlCarousel({
    loop: false,
    margin: 24,
    nav: true,
    autoplay: true,
    autoplayTimeout: 6000,
    items: 4,
    dots: false,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1
      },
      // breakpoint from 768 up
      768: {
        items: 2
      },
      // breakpoint from 1000 up
      1000: {
        items: 4
      }
    }
  });

var gallery_diendan = $('.gallery_diendan');
if (gallery_diendan.length)
  gallery_diendan.owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    items: 1,
    dots: true
  });
var gallery_news_event_2 = $('.gallery_news_event_2');
if (gallery_news_event_2.length)
  gallery_news_event_2.owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    items: 1,
    dots: true
  });

var gallery_htbv = $('.gallery_htbv');
if (gallery_htbv.length)
  gallery_htbv.owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    items: 1,
    dots: true
  });

var gallery_tailieu = $('.gallery_tailieu');
if (gallery_tailieu.length)
  gallery_tailieu.owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    items: 1,
    dots: true
  });
  var gallery_album_tailieu = $('.gallery_album_tailieu');
  if (gallery_album_tailieu.length)
    gallery_album_tailieu.owlCarousel({
      loop: false,
      margin: 0,
      nav: false,
      items: 1,
      dots: true
    });
//
var gallery_3 = $('.gallery-3');
if (gallery_3.length)
  gallery_3.owlCarousel({
    loop: false,
    margin: 24,
    nav: true,
    items: 3,
    dots: false,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1
      },
      // breakpoint from 768 up
      768: {
        items: 2
      },
      // breakpoint from 1000 up
      1000: {
        items: 3
      }
    }
  })

// $('[data-fancybox="open_select1"]').fancybox({
//   touch: false
// });
//


//scroll top
function page_scroll2top() {
  $('html,body').animate({
    scrollTop: 0
  }, '1000');
}
//add button vào body
$("body").append('<div class="button_scroll2top" onclick="page_scroll2top()"></div>');

//keo xuong 1 khoang = chieu cao của màn hình - 500 sẽ hiện button, ngược lại ẩn button
$(window).scroll(function() {
  if ($(window).scrollTop() > (window.innerHeight - 500)) {
    $('.button_scroll2top').fadeIn();
  } else {
    $('.button_scroll2top').fadeOut();
  }
});

// sticky menu top
var menutop_sticky = $('.sticky_header')
if (menutop_sticky.length) {
  $(window).scroll(function() {
    if ($(window).scrollTop() > 0) {
      menutop_sticky.addClass('is-sticky')
    } else {
      menutop_sticky.removeClass('is-sticky')
    }
  });
}
// sub menu sticky on sroll down
function substicky(el) {
  if (el.length) {
    var offsettop = el.offset().top - 50;
    var h_footer = $(".p-footer").height()
    var h_el = el.height();
    var scrollHeight = $(document).height();
    $(window).scroll(function() {
      el.css('min-height', el.height());
      if ($(window).scrollTop() > offsettop) {
        el.addClass('is-sticky');
      //  menutop_sticky.addClass('animated fadeOutUp');
        // menutop_sticky.fadeOut()
      } else {
        el.removeClass('is-sticky');
      //  menutop_sticky.removeClass('fadeOutUp');
      //  menutop_sticky.addClass('fadeIn');
        // menutop_sticky.fadeIn()
      }
      if ($(window).scrollTop() > (scrollHeight - h_footer - h_el)) {
        el.removeClass('is-sticky');
      //  menutop_sticky.removeClass('fade-out');
      }
    });
  }
}

// gọi hàm sticky sub menu
$(window).on("load", function(e) {
  var sticky_submenu = $(".sticky_submenu");
  substicky(sticky_submenu);
})

$(document).on('click', '.icon_drop', function() {
  $(this).next().toggleClass("active");
  $(this).parent().siblings().find("ul").removeClass("active")
})

$(document).on('click', '.icon_menu_mb', function() {
  $('.my_menu').toggleClass("active");
  $(this).toggleClass('icon_close')
})

//
// var delfilter = $(".del_txt")
// if (delfilter.length)
//   delfilter.on("click", function() {
//     $(this).parent().remove();
//   })

// var delall = $(".del_all_txt")
// if (delall.length)
//   delall.on("click", function() {
//     $(this).parent().find("span").remove();
//   })

$(".close_filter_tool, .done_filter_tool").on("click", function() {
  $(this).closest(".filter_tool").removeClass('active');
  $('body').removeClass("ovh");
})

$(".open_filter_tool").on("click", function() {
  $(this).closest(".filter_tool").addClass('active');
  $('body').addClass("ovh");
})



//chi tiet xe mau xe
var item_color = $(".colors .color_item");
if (item_color.length) {
  var item_imglg, item_colordesc, item_price;
  item_color.on('click', function() {
    item_color.removeClass('active');
    $(this).addClass('active');
    item_colordesc = $(this).data('colordesc');
    item_imglg = $(this).data('imglg');
    item_price = $(this).data('price');
    var gallery_color = $(this).closest(".gallery_color");
    gallery_color.find('.color_desc').text(item_colordesc)
    gallery_color.find('.price__txt2').text(item_price)
    gallery_color.find('.img_lg img').attr('src', item_imglg)
  })
}


var slidethumb = $('.slide-dt-thumb');
if (slidethumb.length)
  $('.slide-dt-thumb').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    dots: false,
    mouseDrag: false,
    responsive: {
      0: {
        items: 2,
        margin: 6,
      },
      600: {
        items: 3,
        margin: 6,
      },
      1000: {
        items: 4
      }
    }
  });

var img = $("#img_01");
if (img.length)
  img.elevateZoom();

//pass the images to Fancybox
$(".imgthumb").bind("click", function(e) {
  $(".imgthumb").removeClass('active')
  $(this).addClass('active');
  var imglink = $(this).data('imglg');
  var img = $("#img_01");
  img.attr('src', imglink)
  img.attr('data-zoom-image', imglink);

  //Remove
  $('.zoomContainer').remove();
  img.removeData('elevateZoom');
  img.removeData('zoomImage');

  //Re-create
  img.elevateZoom();

});


//button số lượng page chi tiet san pham
function increase_quantity_up() {
  var x = document.getElementById("myNumber").value;
  x++;
  document.getElementById("myNumber").value = x;
}

function increase_quantity_down() {
  var x = document.getElementById("myNumber").value;
  if (x == 1)
    return;
  x--;
  document.getElementById("myNumber").value = x;
}


var twoDaysFromNow = (new Date().getTime() / 1000) + (86400 * 2) + 1;
var flipdown = new FlipDown(twoDaysFromNow)

    // Start the countdown
    .start()

    // Do something when the countdown ends
    .ifEnded(() => {
      console.log('The countdown has ended!');
    });
